from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed
from flask_login import current_user
from wtforms import StringField, PasswordField, SubmitField, BooleanField, TextAreaField, IntegerField, DateField, SelectField
from wtforms.validators import DataRequired, Length, Email, EqualTo, ValidationError,Regexp
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from flaskDemo.models import User, Team, Project, Case # , Department, getDepartment, getDepartmentFactory
import datetime

team_choice = Team.query.with_entities(Team.team_id).distinct()
team_choices = [(row[0],row[0]) for row in team_choice]
results = list()
for row in team_choice:
    rowDict = row._asdict()
    results.append(rowDict)
my_team_choices = [(row['team_id'],row['team_id']) for row in results]

proj_choice = Project.query.with_entities(Project.project_id).distinct()
proj_choices = [(row1[0],row1[0]) for row1 in proj_choice]
results1=list()
for row1 in proj_choice:
    rowDict1=row1._asdict()
    results1.append(rowDict1)
my_proj_choices = [(row1['project_id'], row1['project_id']) for row1 in results1]

case_choice = Case.query.with_entities(Case.case_id).distinct()
case_choices = [(row1[0],row1[0]) for row1 in case_choice]
results2=list()
for row1 in case_choice:
    rowDict1=row1._asdict()
    results2.append(rowDict1)
my_case_choices = [(row1['case_id'], row1['case_id']) for row1 in results2]


class RegistrationForm(FlaskForm):
    user_id = IntegerField('User Id', validators=[DataRequired(), Length(min=2, max=11)])
    username = StringField('Username', validators=[DataRequired(), Length(min=2, max=20)])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password', validators=[DataRequired(), EqualTo('password')])
    user_first_name = StringField('First Name', validators=[DataRequired(), Length(min=2, max=20)])
    user_last_name = StringField('Last Name', validators=[DataRequired(), Length(min=2, max=20)])
    submit = SubmitField('Sign Up')


def validate_username(self, username):
    user = User.query.filter_by(username=username.data).first()
    if user:
        raise ValidationError('That username is taken. Please choose a different one.')


def validate_email(self, email):
    user = User.query.filter_by(email=email.data).first()
    if user:
        raise ValidationError('That email is taken. Please choose a different one.')


class LoginForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember = BooleanField('Remember Me')
    submit = SubmitField('Login')


class UpdateAccountForm(FlaskForm):
    user_id = IntegerField('User Id', validators=[DataRequired()])
    username = StringField('Username', validators=[DataRequired(), Length(min=2, max=20)])
    email = StringField('Email', validators=[DataRequired(), Email()])
    user_first_name = StringField('First Name', validators=[DataRequired(), Length(min=2, max=20)])
    user_last_name = StringField('Last Name', validators=[DataRequired(), Email()])
    picture = FileField('Update Profile Picture', validators=[FileAllowed(['jpg', 'png'])])
    submit = SubmitField('Update')

    def validate_username(self, username):
        if username.data != current_user.username:
            user = User.query.filter_by(username=username.data).first()
            if user:
                raise ValidationError('That username is taken. Please choose a different one.')

    def validate_email(self, email):
        if email.data != current_user.email:
            user = User.query.filter_by(email=email.data).first()
            if user:
                raise ValidationError('That email is taken. Please choose a different one.')


class CaseForm(FlaskForm):
    case_title = StringField('Title', validators=[DataRequired()])
    case_open_date = DateField('Open Date',validators=[DataRequired()])
    case_close_date = DateField('Close Date',validators=[DataRequired()])
    case_state = StringField('State', validators=[DataRequired()])          #TODO########CHANGE THIS TO CORRECT TYPE
    case_priority = StringField('Priority', validators=[DataRequired()])    #TODO########CHANGE THIS TO CORRECT TYPE
    case_text = TextAreaField('Content', validators=[DataRequired()])
    team_id = SelectField("Team ID", choices=my_team_choices, coerce=int)
    project_id = SelectField("Project ID", choices=my_proj_choices, coerce=int)
    submit = SubmitField('Post')

class CaseUpdateForm(CaseForm):
    case_title = StringField('Title', validators=[DataRequired()])
    case_open_date = DateField('Open Date',validators=[DataRequired()])
    case_close_date = DateField('Close Date',validators=[DataRequired()])
    case_state = StringField('State', validators=[DataRequired()])          #TODO########CHANGE THIS TO CORRECT TYPE
    case_priority = StringField('Priority', validators=[DataRequired()])    #TODO########CHANGE THIS TO CORRECT TYPE
    case_text = TextAreaField('Content', validators=[DataRequired()])
    team_id = SelectField("Team ID", choices=my_team_choices, coerce=int)
    project_id = SelectField("Project ID", choices=my_proj_choices, coerce=int)


class ReplyForm(FlaskForm):
    case_id = SelectField("Case ID", choices=case_choices, coerce=int)
    reply_text = StringField('Content')
    submit = SubmitField('Post')

class ReplyUpdateForm(ReplyForm):
    case_id = SelectField("Case ID", choices=case_choices, coerce=int)
    reply_text = StringField('Content')


class Can_Include(FlaskForm):
    #TODO########-----SELECTFILED---ADD DROP DOWN FOR CASE ID AND DO SOMETHING ABOUT THE CURRENTLY LOGGED IN USER BEING THE user_id
    case_id = IntegerField('Case ID', validators=[DataRequired()])
    #TODO########-----SELECTFIELD---ADD DROP DOWN FOR CASE ID AND DO SOMETHING ABOUT THE CURRENTLY LOGGED IN USER BEING THE user_id
    project_id = SelectField('Project ID', validators=[DataRequired()], choices=proj_choices, coerce =int)

    submit = SubmitField('Post')


