import os
import secrets
from PIL import Image
from flask import render_template, url_for, flash, redirect, request, abort
from sqlalchemy.orm import Query, Session
from flaskDemo import app, db, bcrypt
from flaskDemo.forms import RegistrationForm, LoginForm, UpdateAccountForm, Can_Include, CaseUpdateForm, ReplyUpdateForm
from flaskDemo.forms import CaseForm, ReplyForm, RegistrationForm, LoginForm, UpdateAccountForm
from flaskDemo.models import User, Assigned, Can_include, Case, Project, Team, Works_on, Reply
from flask_login import login_user, current_user, logout_user, login_required
from sqlalchemy import func
import datetime
from datetime import date


@app.route("/")
@app.route("/home")
def home():

    cases_join = Case.query.join(Can_include, Case.case_id == Can_include.case_id) \
         .add_columns(Case.case_id, Case.case_title, Case.case_open_date, Case.case_state) \
         .join(Project, Project.project_id == Can_include.project_id).add_columns(Project.project_title, Project.project_id) \
         .join(Assigned, Assigned.project_id == Project.project_id) \
         .join(Team, Team.team_id == Assigned.team_id).add_columns(Team.team_name, Team.team_id) \
         .join(Works_on, Works_on.team_id == Assigned.team_id) \
         .join(User, User.user_id == Case.user_id).add_columns(User.user_first_name, User.user_last_name, User.user_id).order_by(Case.case_id.desc())


    # cases_join = Case.query.join(Can_include, Case.case_id == Can_include.case_id) \
    #     .add_columns(Case.case_id, Case.case_title, Case.case_open_date, Case.case_state, Can_include.project_id) \
    #     .join(Project, Project.project_id == Can_include.project_id).add_columns(Project.project_title) \
    #     .join(Assigned, Assigned.project_id == Project.project_id).add_columns(Assigned.team_id) \
    #     .join(Team, Team.team_id == Assigned.team_id).add_columns(Team.team_name) \
    #     .join(Works_on, Works_on.team_id == Assigned.team_id).add_columns(Works_on.user_id) \
    #     .join(User, User.user_id == Works_on.user_id).add_columns(User.user_first_name, User.user_last_name)

    # print(current_user.user_id)
    # is_user_admin = 0
    #if 0 == current_user.user_id:
    #   is_user_admin = 1

    return render_template('case_home.html', cases_join=cases_join, current_user=current_user)  #is_user_admin=is_user_admin


@app.route("/case/<case_id>")
def case(case_id):
    case = Case.query.get_or_404(case_id)

    users_join = User.query.join(Case, Case.user_id == User.user_id)\
        .add_columns(Case.user_id, User.user_first_name, User.user_last_name).filter_by(case_id=case.case_id)
    # creating_user = User.query.get_or_404(case.user_id)
    # print(creating_user)

    # get the associated replies by filter and then order them by the reply_id as the number is always increasing.
    # this allows us to not need to keep track of precise posting time and figuring out the order of the posts
    # by the exact time they were posted.
    replies = Reply.query.filter_by(case_id=case.case_id).order_by(Reply.reply_id)

    users = list()
    for reply in replies:
        print(reply.user_id)
        user = User.query.get_or_404(reply.user_id)
        # user = User.query.filter_by(user_id=reply.user_id)
        users.append(user)



    return render_template('case.html',
                           title=case.case_title,
                           case=case,
                           users_join = users_join,
                           replies=replies,
                           users=users,
                           now=date.today())


@app.route("/about")
def about():
    return render_template('about.html', title='About')


@app.route("/register", methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = RegistrationForm()
    if form.validate_on_submit():
        hashed_password = form.password.data # bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user = User(user_id=form.user_id.data,
                    username=form.username.data,
                    email=form.email.data,
                    password=hashed_password,
                    user_first_name=form.user_first_name,
                    user_last_name=form.user_last_name)
        db.session.add(user)
        db.session.commit()
        flash('Your account has been created! You are now able to log in', 'success')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)


@app.route("/login", methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user : # #and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user, remember=form.remember.data)
            next_page = request.args.get('next')
            return redirect(next_page) if next_page else redirect(url_for('home'))
        else:
            flash('Login Unsuccessful. Please check email and password', 'danger')
    return render_template('login.html', title='Login', form=form)


@app.route("/logout")
def logout():
    logout_user()
    return redirect(url_for('home'))


@app.route("/account", methods=['GET', 'POST'])
@login_required
def account():
    form = UpdateAccountForm()
    if form.validate_on_submit():

        current_user.user_id = form.user_id.data
        current_user.username = form.username.data
        current_user.email = form.email.data
        current_user.user_first_name = form.user_first_name.data
        current_user.user_last_name = form.user_last_name.data

        db.session.commit()
        flash('Your account has been updated!', 'success')
        return redirect(url_for('account'))
    elif request.method == 'GET':

        form.user_id.data = current_user.user_id
        form.username.data = current_user.username
        form.email.data = current_user.email
        form.user_first_name.data = current_user.user_first_name
        form.user_last_name.data = current_user.user_last_name

    # image_file = url_for('static', filename='profile_pics/' + current_user.image_file)
    return render_template('account.html', title='Account', form=form) #  image_file=image_file,


#######################################################################################################################
###---------------------------------------------------CASES---------------------------------------------------------###
#######################################################################################################################
@app.route("/case_assign/new", methods=['GET', 'POST'])
@login_required
def new_can_incl():
    form = Can_Include()
    if form.validate_on_submit():
        _can_inc = Can_include(case_id=form.case_id.data,
                               project_id=form.project_id.data)
        db.session.add(_can_inc)
        db.session.commit()
        flash('You have added a new whatever!', 'success')
        return redirect(url_for('home'))
    return render_template('create_assigned.html', title='New Can Include', form=form, legend='New Can Include')


@app.route("/case/new", methods=['GET', 'POST'])
@login_required
def new_case():

    form = CaseForm()
    if form.validate_on_submit():
        # finds the highest value case and adds one to be the new id
        highest_case = int(db.session.query(func.max(Case.case_id)).scalar()) + 1

        print(highest_case)

        _case = Case(case_id=highest_case,
                     case_title=form.case_title.data,
                     case_open_date=form.case_open_date.data,
                     case_close_date=form.case_close_date.data,
                     case_state=form.case_state.data,
                     case_priority=form.case_priority.data,
                     case_text=form.case_text.data,
                     user_id=current_user.user_id)

        _can_inc = Can_include(case_id=highest_case, project_id=form.project_id.data)
        _works_on = Works_on(team_id=form.team_id.data, user_id=current_user.user_id)


        db.session.add(_case)
        db.session.commit()


        cases = Can_include.query.all()
        add = True
        for case in cases:
            if case.case_id == highest_case and cases.project_id == form.project_id.data:
                add = False
        if add:
            db.session.add(_can_inc)




        teams = Works_on.query.all()
        add = True
        for team in teams:
            if team.user_id == current_user.user_id and team.team_id == form.team_id.data:
                add = False

        if add:
            db.session.add(_works_on)


        print("TEAM : ", form.team_id.data)
        print("PROJECT : ", form.project_id.data)
        print("USER  : ", current_user.user_id)

        db.session.commit()




        flash('You have added a new case with team!', 'success')
        return redirect(url_for('home'))
    return render_template('create_case.html', title='New Case', form=form, legend='New Case')


@app.route("/case/<case_id>/update", methods=['GET', 'POST'])
@login_required
def update_case(case_id):
    _case = Case.query.get_or_404(case_id)
    current_case = _case.case_title
    form = CaseUpdateForm()
    if form.validate_on_submit():
        if current_case!= form.case_title.data:
            _case.case_title = form.case_title.data
        _case.case_open_date = form.case_open_date.data
        _case.case_close_date = form.case_close_date.data
        _case.case_state = form.case_state.data
        _case.case_priority = form.case_priority.data
        _case.case_text = form.case_text.data

        db.session.commit()
        flash('Your case has been updated!', 'success')
        return redirect(url_for('case', case_id =_case.case_id))  # TODO#### might need to be case.case_id
    elif request.method == 'GET':
        form.case_title.data = _case.case_title
        form.case_open_date.data = _case.case_open_date
        form.case_close_date.data = _case.case_close_date
        form.case_state.data = _case.case_state
        form.case_priority.data = _case.case_priority
        form.case_text.data = _case.case_text
    return render_template('update_case.html', title='Update Case',
                           form=form, legend='Update Case')




@app.route("/case/<case_id>/delete", methods=['GET','POST'])
@login_required
def delete_case(case_id):
    _case = Case.query.get_or_404(case_id)

    # _can_incl = Can_include.query.filter_by(case_id=case_id).first()

    # _can_incl = Can_include.query.filter_by(case_id=_case.case_id)   # .order_by(Reply.reply_id)

    Can_include.query.filter_by(case_id=_case.case_id).delete()
    db.session.delete(_case)
    db.session.commit()
    flash('The case has been deleted!', 'success')
    return redirect(url_for('home'))

#######################################################################################################################
###--------------------------------------------------REPLIES--------------------------------------------------------###
#######################################################################################################################

@app.route("/reply/new", methods=['GET', 'POST'])
@login_required
def new_reply():
    form = ReplyForm()
    if form.validate_on_submit():
        print("REW_REPLY INSIDE MANAGEMENT")

        print("reply_date : ", date.today())
        print("case_id : ", form.case_id.data)
        print("reply_text : ", form.reply_text.data)
        print("user_id : ", current_user.user_id)

        highest_reply = int(db.session.query(func.max(Reply.reply_id)).scalar()) + 1

        reply = Reply(reply_id=highest_reply,
                      reply_date=date.today(),
                      case_id=form.case_id.data,
                      reply_text=form.reply_text.data,
                      user_id=current_user.user_id)

        db.session.add(reply)
        db.session.commit()
        flash('You have added a new reply!', 'success')
        return redirect(url_for('home'))
    return render_template('create_reply.html', title='New Reply', form=form, legend='New Reply')


@app.route("/reply/<reply_id>/update", methods=['GET', 'POST'])
@login_required
def update_reply(reply_id):
    reply = Reply.query.get_or_404(reply_id)
    current_reply = Reply.reply_text
    form = ReplyUpdateForm()
    if form.validate_on_submit():
        if current_reply != form.reply_text.data:
            reply.reply_text = form.reply_text.data
        reply.case_id = form.case_id.data
        db.session.commit()
        flash('Your reply has been updated!', 'success')
        return redirect(url_for('case', case_id=reply.case_id))

    elif request.method == 'GET':
        form.reply_text.data = reply.reply_text
    return render_template('update_reply.html', title='Update Reply', form=form, legend='Update Reply')


@app.route("/reply/<reply_id>/delete", methods=['POST'])
@login_required
def delete_reply(reply_id):
    reply = Reply.query.get_or_404(reply_id)
    db.session.delete(reply)
    db.session.commit()
    flash('The reply has been deleted!', 'success')
    return redirect(url_for('home'))
