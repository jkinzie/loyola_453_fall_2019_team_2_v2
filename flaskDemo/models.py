from datetime import datetime
from flaskDemo import db, login_manager
from flask_login import UserMixin
from functools import partial
from sqlalchemy import orm

db.Model.metadata.reflect(db.engine)


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class User(db.Model, UserMixin):
    __table__ = db.Model.metadata.tables['user']

    def get_id(self):
        return self.user_id

    def __init__(self, user_id, username, email, password, user_first_name, user_last_name):

        # self.id = user_id
        self.user_id = user_id
        self.username = username
        self.email = email
        self.password = password
        self.user_first_name = user_first_name
        self.user_last_name = user_last_name

    def __repr__(self):

        return f"User('{self.username}','{self.email}')"


class Assigned(db.Model):
    __table__ = db.Model.metadata.tables['assigned']


class Can_include(db.Model):
    __table__ = db.Model.metadata.tables['can_include']

    def __init__(self, project_id, case_id):
        self.case_id = case_id
        self.project_id = project_id

    def __repr__(self):
        return f"Can_include('{self.case_id}','{self.project_id}')"


class Case(db.Model):
    __table__ = db.Model.metadata.tables['case']

    def __init__(self, user_id, case_id, case_title, case_open_date, case_close_date, case_state, case_priority, case_text):
        self.case_id = case_id
        self.case_title = case_title
        self.case_open_date = case_open_date
        self.case_close_date = case_close_date
        self.case_state = case_state
        self.case_priority = case_priority
        self.case_text = case_text
        self.user_id = user_id


class Reply(db.Model):
    __table__ = db.Model.metadata.tables['reply']

    def __init__(self, reply_id, user_id, case_id, reply_date, reply_text):
        self.reply_id = reply_id
        self.reply_date = reply_date
        self.reply_text = reply_text
        self.user_id = user_id
        self.case_id = case_id


class Project(db.Model):
    __table__ = db.Model.metadata.tables['project']


class Team(db.Model):
    __table__ = db.Model.metadata.tables['team']


class Works_on(db.Model):
    __table__ = db.Model.metadata.tables['works_on']

    def __init__(self, team_id, user_id):
        self.team_id = team_id
        self.user_id = user_id
